
# Simple CRUD Frontend
Frontend for crud employees system using Angular


## Installation

Clone the project

```bash
  git clone https://gitlab.com/bananaqib/simple_crud_frontend
```

Go to the project directory

```bash
  cd <project-directory>
```

Install dependencies

```bash
 npm install
```

Run the project 

```bash
 ng serve
```
