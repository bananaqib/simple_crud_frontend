export interface Post {
    
    id: number;
    employee_id: string;
    first_name: string;
    last_name: string;      
    email: string;         
    phone: string;          
    hire_date: string;      
    job_title: string;      
    manager_id: string;     

}
